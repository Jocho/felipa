<?php

use Felipa\Autoloader;
use Felipa\Felipa;
use Felipa\FelipaException;

define('ROOT', __DIR__);

require(ROOT . '/Felipa/Autoloader.php');

Autoloader::init(ROOT, false);

try {
    $app = new Felipa();
    $app->setEnvironment(Felipa::ENV_DEV);

    if (php_sapi_name() === 'cli') {
        $app->cmd();
        exit;
    }
    
    $app->run();
}
catch(FelipaException $e) {
    echo json_encode([
        'status' => 'error',
        'error' => $e->getMessage()
    ]);
}
catch(Exception $e) {
    echo '<pre>';
    echo implode("\n", [$e->getMessage(), $e->getFile(), $e->getLine()]);
    echo '</pre>';
}
