<?php

namespace Felipa;

use \Exception;

/**
 * Default exception thrown by Felipa Framework.
 */
class FelipaException extends Exception
{

}