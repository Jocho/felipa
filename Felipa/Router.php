<?php

namespace Felipa;

class Router
{
    private const ROUTES_FILEPATH = ROOT . DIRECTORY_SEPARATOR . 'routes.php';

    private string $controllerName;
    private ?string $methodName = null;
    private array $vars = [];
    private ?object $callback;

    /**
     * Sets the values of method, path and query string so they can be parsed and used later.
     *
     * @param Method $method
     * @param string $path
     * 
     * @throws FelipaException
     */
    public function __construct(
        private readonly Method $usedMethod,
        private readonly string $path
    )
    {
        $routes = require_once(self::ROUTES_FILEPATH);
        $evaluatedRoutes = $this->evaluateRoutes($routes);
        $controllerName = $methodName = null;

        if (empty($evaluatedRoutes)) {
            throw new FelipaException('Zero routes evaluated.');
        }

        usort($evaluatedRoutes, function($x, $y) {
            return $x['value'] < $y['value'];
        });

        if (0 === $evaluatedRoutes[0]['value']) {
            throw new FelipaException('No suitable route matched.');
        }

        $this->vars = $evaluatedRoutes[0]['vars'];

        if (is_callable($evaluatedRoutes[0]['ctrl'])) {
            $this->controllerName = 'anonymous_' . time();
            $this->callback = $evaluatedRoutes[0]['ctrl'];
            return;
        }

        [$controllerName, $methodName] = explode(':', $evaluatedRoutes[0]['ctrl']);

        $this->controllerName = $controllerName;
        $this->methodName = $methodName;
    }

    /**
     * Evaluates given route and tries to match the most suitable path in routes.php.
     *
     * Paths written in the routes.php have following structure:
     *
     * `METHOD /speciffic/path` - relative path that is going to be resolved
     * `METHOD /{variable}      - path with variable
     * `METHOD /*               - path that matches any value, but the value is not processed into variable
     *
     * The evaluation is processed as follows:
     *
     * 1. path is divided by forward slash into an array
     * 2. each type of the path part is evaluated:
     *      - speciffic path .... 4
     *      - variable  ......... 2
     *      - `wild` operator ... 1
     *
     * All the values are summed-up and the result is score of the path.
     * The path with the highest score is chosen. In case of multiple paths share the score, the former path is chosen.
     *
     * @param array $routes
     * @param ?Method $method
     * @param string $prefix
     * @return array
     */
    private function evaluateRoutes(array $routes, ?Method $method = null, string $prefix = ''): array
    {
        static $evaluatedRoutes = [];

        foreach ($routes as $route => $ctrl) {
            preg_match(Regex::METHOD_PATH->value, $route, $matches);

            $usedMethod = null !== $method ? $method : Method::tryFrom((string) $matches['method']);
            $usedPath   = $prefix . ($matches['path'] ?? '');

            if (is_array($ctrl)) {
                $this->evaluateRoutes($ctrl, $usedMethod, $usedPath);
                continue;
            }

            if ($usedMethod !== $this->usedMethod) {
                continue;
            }

            $usedPath = preg_replace(Regex::STRIP_SLASH->value, '$1', $usedPath);
            
            $exr = explode('/', $usedPath);

            $hasWildEnd = end($exr) === '*';
            $parsedPath = explode('/', $this->path);

            $evaluatedRoute = [
                'fp' => DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $exr),
                'ctrl' => $ctrl,
                'vars' => [],
                'value' => 0,
            ];

            $evaluatedRoute['value'] = (int) $hasWildEnd;

            if (count($exr) === count($parsedPath)) {
                foreach ($parsedPath as $k => $pathPart) {
                    if ($exr[$k] === $pathPart) {
                        $evaluatedRoute['value'] += 4;
                    }
                    elseif (preg_match(Regex::ROUTE_VARIABLE->value, $exr[$k], $matches)) {
                        $evaluatedRoute['value'] += 2;
                        $evaluatedRoute['vars'][$matches[1]] = $pathPart;
                    }
                    elseif ($exr[$k] === '*') {
                        $evaluatedRoute['value'] += 1;
                    }
                    elseif (!$hasWildEnd) {
                        $evaluatedRoute['value'] = 0;
                        break;
                    }
                }
            }

            $evaluatedRoutes[] = $evaluatedRoute;
        }

        return $evaluatedRoutes;
    }

    /**
     * Returns the name of controller to be used.
     *
     * @return string
     */
    public function getControllerName(): string
    {
        return $this->controllerName;
    }

    /**
     * Returns the name of controller's method to be used.
     *
     * @return string|null
     */
    public function getMethodName(): ?string
    {
        return $this->methodName;
    }

    /**
     * Returns parsed variables passed via URL in curly brackets
     *
     * @return array
     */
    public function getVars(): array
    {
        return $this->vars;
    }

    /**
     * Returns callback, if any is set in the routes.php near matching route.
     *
     * @return callable
     */
    public function getCallback(): callable
    {
        return $this->callback;
    }
}
