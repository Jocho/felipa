<?php

namespace Felipa;

class Request
{
    private Session $session;
    private array $headers = [];
    private array $files = [];
    private array $query = [];
    private ?array $inputs = null;

    /**
     * Constructor for the Request.
     *
     * @param Method $method
     * @param string $path
     * @param string $queryString
     * @param array $vars
     */
    public function __construct(
        private Method $method,
        private string $path = '/',
        private string $queryString = '',
        private array $vars = [],
    ) {
        $this->headers = getallheaders();
        
        if (in_array($method, [Method::POST, Method::PUT, Method::PATCH, Method::DELETE])) {
            $body = file_get_contents('php://input');

            if (false !== $body) {
                $this->inputs = json_decode($body, true);

                if (null === $this->inputs) {
                    parse_str($body, $this->inputs);
                }
            }

            if (empty($this->inputs)) {
                $this->inputs = $_POST;
            }
        }

        if (!empty($_FILES)) {
            $this->files = $this->parseFileData($_FILES);
        }

        parse_str($queryString, $this->query);

        $this->session = new Session();

        if ($this->session->has('VARS')) {
            $this->addVars($this->session->get('VARS'));
            $this->session->del('VARS');
        }
    }

    public function getMethod(): Method
    {
        return $this->method;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getParsedPath(): array
    {
        return explode('/', $this->path);
    }

    public function getQuery(): array
    {
        return $this->query;
    }

    public function setVars(array $vars): void
    {
        $this->vars = $vars;
    }

    public function addVars(array $vars): void
    {
        $this->vars = array_merge($this->vars, $vars);
    }

    public function getVars(): array
    {
        return $this->vars;
    }

    public function setInputs(array $vars): void
    {
        $this->inputs = $vars;
    }

    public function getInputs(): ?array
    {
        return $this->inputs;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setSession(Session $session): void
    {
        $this->session = $session;
    }

    public function getSession(): Session
    {
        return $this->session;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * Prepares file data from $_FILESS if any files were uploaded.
     *
     * @param array $srcFiles
     * @return void
     */
    private function parseFileData(array $srcFiles) {
        $files = [];
        foreach ($srcFiles as $variable => $data) {
            $d = [];
            foreach ($data as $param => $value) {
                $value = (is_array($value)) ? $value : [$value];

                foreach ($value as $k => $val) {
                    $d[$k][$param] = $val;
                }
            }

            $files[$variable] = $d;
        }
        return $files;
    }
}
