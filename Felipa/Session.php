<?php

namespace Felipa;

use JsonSerializable;

final class Session implements JsonSerializable
{
    private array $data = [];

    public function jsonSerialize(): array
    {
        return $this->data;
    }

    public function __construct() {
        session_start();

        $this->data = $_SESSION;
    }

    public function flush(): self
    {
        $this->data = [];
        return $this;
    }

    public function commit(): void
    {
        $_SESSION = json_decode(json_encode($this->data), true);
        // return session_write_close();
    }

    public function set(string $variable, $value): self
    {
        $this->data[$variable] = $value;
        return $this;
    }

    public function get(string $variable)
    {
        return $this->data[$variable];
    }

    public function has(string $variable): bool
    {
        return isset($this->data[$variable]);
    }

    public function del(string $variable): self
    {
        unset($this->data[$variable]);
        return $this;
    }

    public function isEmpty(): bool
    {
        return empty($this->data);
    }

    public function all(): array
    {
        return $this->data;
    }
}