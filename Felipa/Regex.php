<?php

namespace Felipa;

enum Regex: string
{
    case METHOD_PATH = '/^(?<method>[A-Z]+)?( *(?<path>\/.*))?$/';
    case STRIP_SLASH = '/^\/(.*?)\/?$/';
    case ROUTE_VARIABLE = '/\{(.+?)\}/';
    case CMD_ARGUMENTS = '/^(?<ctrl>.*):(?<method>.*)$/';
    case CALLBACK_FUNC = '/^anonymous_\d+$/';
}