<?php

namespace Felipa;

class Templater {
    private string $classPath;
    private string $snippet;
    private array $vars;

    public function __construct(
        string $classPath,
        string $snippet = '',
        array $vars = []
    ) {
        $this->classPath = rtrim($classPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->snippet = $snippet;
        $this->vars = $vars;
    }

    /**
     * @param array $vars
     * @return self
     */
    public function setAll(array $vars): self
    {
        $this->vars = $vars;
        return $this;
    }

    /**
     * @param string $var
     * @param $val
     * @return self
     */
    public function set(string $var, $val): self
    {
        $this->vars[$var] = $val;
        return $this;
    }

    /**
     * @param string $var
     */
    public function get(string $var)
    {
        return $this->vars[$var] ?? null;
    }

    /**
     * @param string $var
     * @return boolean
     */
    public function has(string $var): bool
    {
        return isset($this->vars[$var]);
    }

    /**
     * @param string $snippet
     * @return self
     */
    public function setSnippet(string $snippet): self
    {
        $this->snippet = $snippet;
        return $this;
    }

    /**
     * @return string
     */
    public function getSnippet(): string
    {
        return $this->snippet;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->classPath . $this->snippet . '.html';
    }

    /**
     * @return boolean
     */
    public function exists(): bool
    {
        return file_exists($this->getPath());
    }

    /**
     * @return string
     */
    public function render(): string
    {
        if (!$this->exists()) {
            throw new FelipaException(
                sprintf(
                    'Could not find HTML snippet `%s` in address `%s`.',
                    $this->snippet,
                    $this->getPath()
                )
            );
        }
        ob_start();
        extract($this->vars);
        include($this->getPath());

        $response = ob_get_clean();

        return (false !== $response) ? $response : '';
    }
}