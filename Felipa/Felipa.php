<?php

namespace Felipa;

use ReflectionFunction;
use ReflectionMethod;

class Felipa
{
    public const INSTANCE = 1;
    public const CONFIG = 2;
    public const ENV_DEV = 'development';
    public const ENV_PROD = 'production';

    private array $config = [];
    private ?Request $request = null;
    private ?Response $response = null;

    public function __construct(
        private string $environment = self::ENV_DEV,
        private string $configPath = 'config'
    ) {}

    /**
     * Load the config stored in the /config folder. Files have to be named config.global.php and
     * config.<ENVIRONMENT>.php, where <ENVIRONMENT> can be set during launch of an application.
     *
     * @return void
     */
    private function loadConfig()
    {
        if (!is_dir(ROOT . DIRECTORY_SEPARATOR . $this->configPath)) {
            throw new FelipaException('Could not find `config.global.php` or `config.' . $this->environment . '.php` file.');
        }

        foreach (scandir($this->configPath) as $config) {
            if (in_array($config, ['.', '..'])) {
                continue;
            }

            $path = $this->configPath . DIRECTORY_SEPARATOR . $config;

            if (file_exists($path) && is_file($path)) {
                if (preg_match('/\.global.php/', $config) || preg_match('/\.' . $this->environment . DIRECTORY_SEPARATOR, $config)) {
                    $this->config = require_once($path);
                }
            }
        }

        if (empty($this->config)) {
            throw new FelipaException('Could not find `config.global.php` or `config.' . $this->environment . '.php` file.');
        }
    }

    /**
     * Sets the variable that tells the application, which environment is currently used.
     *
     * @param string $environment
     * @return void
     */
    public function setEnvironment(string $environment)
    {
        if (!in_array($environment, [self::ENV_DEV, self::ENV_PROD])) {
            throw new FelipaException('Unknown environment `' . $environment . '` set.');
        }
        $this->environment = $environment;
    }

    /**
     * Gets current value of an environment.
     *
     * @return string
     */
    public function getEnvironment(): string
    {
        return $this->environment;
    }

    /**
     * Returns stored array of config
     *
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * Launches the application in the CLI mode.
     *
     * Usage: `php index.php controller:method`
     *
     * Every route launchable via commandline has to be defined in the config array under key `commandline`.
     *
     * @return void
     * @throws FelipaException
     */
    public function cmd()
    {
        if (php_sapi_name() !== 'cli') {
            throw new FelipaException('Not in CLI environment, terminating.');
        }

        $arguments = $_SERVER['argv'];

        if (is_string($arguments)) {
            parse_str($_SERVER['argv'], $arguments);
        }

        $this->loadConfig();

        if (!isset($arguments[1])) {
            throw new FelipaException('No controller:method was called.', 500);
        }
        $args = array_slice($arguments, 2);

        preg_match(Regex::CMD_ARGUMENTS->value, $arguments[1], $match);

        if (!isset($this->config['commandline'][$match['ctrl']])) {

        }

        $ctrlName = $this->config['commandline'][$match['ctrl']];
        $ctrl = $this->factorize($ctrlName);

        if (!method_exists($ctrl, $match['method'])) {
            throw new FelipaException('Unknown method `' . $match['method'] . '` called in controller `' . $ctrlName . '`.');
        }

        $ctrl->{$match['method']}($args);
    }

    /**
     * Runs the application in the default mode.
     * The route is parsed and corresponding controller/method called based on the routes defined in the `/routes.php`.
     *
     * @return void
     * @throws FelipaException
     */
    public function run()
    {
        $this->loadConfig();

        if (!file_exists('routes.php')) {
            throw new FelipaException('File `routes.php` not found.');
        }

        $pathQuery = explode('?', $_SERVER['REQUEST_URI']);
        $path = preg_replace(Regex::STRIP_SLASH->value, '$1', $pathQuery[0] ?? '/');
        $queryString = $pathQuery[1] ?? '';

        $method = Method::tryFrom(strtoupper($_SERVER['REQUEST_METHOD']));
        
        if (null === $method) {
            throw new FelipaException('Unknown request method.');
        }

        $router = new Router($method, $path);

        $this->request = new Request(
            $method,
            $path,
            $queryString,
            $router->getVars(),
        );

        $this->response = new Response($this->config);
        $this->response = $this->prepareResponse($router);
        $this->response->makeResponse($this->request->getSession());
    }

    /**
     * Prepares the response on whether the controller name is invoking anonymous function or not.
     *
     * @param Router $router
     * @return Response
     */
    private function prepareResponse(Router $router): Response
    {
        $ctrlName = $router->getControllerName();
        $methodName = $router->getMethodName();

        if (preg_match(Regex::CALLBACK_FUNC->value, $ctrlName) && $methodName === null) {
            $callback = $router->getCallback();

            return $callback(...$this->filterParameters(
                new ReflectionFunction($callback),
                [$this->request, $this->response]
            ));
        }

        $ctrl = $this->factorize($ctrlName);

        if (!method_exists($ctrl, $methodName)) {
            throw new FelipaException('Unknown method `' . $methodName . '` called in controller `' . $ctrlName . '`.');
        }

        return $ctrl->{$methodName}(...$this->filterParameters(
            new ReflectionMethod($ctrl, $methodName),
            [$this->request, $this->response]
        ));
    }

    private function filterParameters(\ReflectionFunctionAbstract $refl, array $availableParameters): array
    {
        $parameters = [];

        foreach ($refl->getParameters() as $parameter) {
            $paramType = $parameter->getType()->getName();
            
            foreach ($availableParameters as $availParam) {
                $refl = new \ReflectionNamedType($availParam);
                if (get_class($availParam) === $paramType) {
                    $parameters[] = $availParam;
                    break;
                }
            }
        }

        return $parameters;
    }

    /**
     * Applies dependencies during constructing instances of targeted classes.
     * When a class is mentioned in the config, 'dependencies' key, every class or variable mentioned in the config
     * is loaded first in order to be put as an argument into the controller constructor.
     *
     * @param string $className
     * @return object
     */
    private function factorize(string $className)
    {
        if (!isset($this->config['dependencies'][$className])) {
            return new $className();
        }

        $args = [];

        foreach ($this->config['dependencies'][$className] as $dependency) {
            $args[] = match($dependency) {
                self::INSTANCE => $this,
                self::CONFIG => $this->config,
                default => $this->factorize($dependency),
            };
        }

        return new $className(... $args);
    }

    /**
     * Returns instance of current Request, if is present.
     *
     * @return Request|null
     */
    public function getRequest(): ?Request
    {
        return $this->request;
    }

    /**
     * Returns instance of current Response, if is present.
     *
     * @return Response|null
     */
    public function getResponse(): ?Response
    {
        return $this->response;
    }
}
