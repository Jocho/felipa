<?php

namespace Felipa;

enum ResponseMethod: string
{
    case HTML = 'html';
    case JSON = 'json';
    case STREAM = 'stream';
    case CUSTOM = 'custom';
}