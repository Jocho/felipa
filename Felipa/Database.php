<?php
namespace Felipa;

use \SQLite3;

class Database
{
    public const TYPE_YAML = 1;
    public const TYPE_JSON = 2;

    private string $path;
    private int $type;
    private ?string $connector = null;
    private ?array $data = null;

    /**
     * @param array $settings
     * @throws FelipaException
     */
    public function __construct(array $settings)
    {
        if (null === $settings['database']['host']) {
            throw new FelipaException('No path to database directory was given (database.host).');
        }

        $this->path = $settings['database']['host'];
        $this->type = $settings['database']['type'];


        if (!is_dir($this->path)) {
            mkdir($this->path);

            if (!is_dir($this->path)) {
                throw new FelipaException(sprintf('Database directory `%s` could not be created.', $this->path));
            }
        }
    }

    /**
     * Checks whether any connector is opened.
     * 
     * @return bool
     */
    public function isOpened(): bool
    {
        return null !== $this->connector && null !== $this->data;
    }

    private function encode(?array $data = null): string
    {
        return null !== $data ? json_encode($data) : [];
    }

    private function decode(?string $data = null): array
    {
        return null !== $data ? json_decode($data, true) : null;
    }

    private function extension(): string
    {
        return 'json';
    }

    /**
     * Opens a connector with given name. If the storage file does not exist, it is created.
     * 
     * @param string $connector
     * @return self
     */
    public function open(string $connector): self
    {
        $connectorPath = $this->path . DIRECTORY_SEPARATOR . $connector . '.' . $this->extension();

        if (!file_exists($connectorPath)) {
            file_put_contents($connectorPath, $this->encode([]));

            if (!file_exists($connectorPath)) {
                throw new FelipaException(sprintf('Cannot create a connector file in `%s`.', $connectorPath));
            }
        }

        $this->connector = $connector;
        $this->data = $this->decode(file_get_contents($connectorPath));

        if (!is_array($this->data)) {
            throw new FelipaException(sprintf('Data in connector `%s` are malformed.', $connectorPath));
        }

        return $this;
    }

    /**
     * Attempts to return path to the connector, if the connector is opened.
     * 
     * @return string
     */
    private function getConnectorPath(): string
    {
        if ($this->isOpened()) {
            return $this->path . DIRECTORY_SEPARATOR . $this->connector . '.' . $this->extension();
        }

        throw new FelipaException('Cannot get connector path. Database path or connector name is not set.');
    }

    /**
     * Gets the value stored in the database based on the key, or returns whole list if no key is provided.
     *
     * @param string|null $key
     * @return mixed
     * @throws FelipaException
     */
    public function get(?string $key = null)
    {
        if (!$this->isOpened()) {
            throw new FelipaException('No connector opened, have no data to grab from.');
        }

        if (null === $key) {
            return $this->data;
        }

        return $this->has($key) ? $this->data[$key] : null;
    }

    /**
     * Checks whether the value with given key exists or not.
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        if (!$this->isOpened()) {
            throw new FelipaException('No connector opened, do not know, where should I search.');
        }
        return isset($this->data[$key]);
    }

    /**
     * Sets the value to the given key.
     *
     * @param string $key
     * @param mixed $value
     * @return self
     */
    public function set(string $key, $value): self
    {
        if (!$this->isOpened()) {
            throw new FelipaException('No connector opened, do not know, where should I save the record.');
        }
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * Creates new record in the connector database and returns the key of the record.
     *
     * @param mixed $value
     * @return string uniqid
     */
    public function add($value): string
    {
        if (!$this->isOpened()) {
            throw new FelipaException('No connector opened, do not know, where should I add the record.');
        }

        do {
            $id = uniqid();
        } while (isset($this->data[$id]));

        $this->data[$id] = $value;

        return $id;
    }

    /**
     * Removes record from the database by given key.
     *
     * @param string $key
     * @return bool
     */
    public function del(string $key): bool
    {
        if ($this->isOpened() && $this->has($key)) {
            unset($this->data[$key]);
            return true;
        }
        return false;
    }

    /**
     * Searches for the records that match searched conditions.
     * 
     * @param array $conditions
     * @return array
     */
    public function find(array $conditions): array
    {
        return array_filter($this->data, function ($x) use ($conditions) {
            $ok = 0;
            foreach ($conditions as $key => $searched) {
                $kno = self::parseCondition($key);

                if (!isset($x[$kno['key']])) {
                    return false;
                }

                $ok += self::evalCondition($x[$kno['key']], $searched, $kno['operator'], $kno['not']) ? 1 : 0;
            };

            return $ok === count($conditions);
        });
    }

    /**
     * Parses given search key into actual key, negator, and operator 
     * 
     * @param string $key
     * @return array
     */
    private static function parseCondition(string $key): array
    {
        preg_match('/^(?<key>.+?)( (?<not>\!)?(?<operator>.+))?$/', $key, $match);

        return [
            'key' => $match['key'],
            'not' => strlen($match['not']) > 0,
            'operator' => $match['operator'] ?? '=',
        ];
    }

    /**
     * Evaluates the condition based on the operator and `not` negator.
     * 
     * @param mixed $stored
     * @param mixed $searched
     * @param string $operator
     * @param bool $not
     */
    private static function evalCondition($stored, $searched, string $operator, bool $not = false)
    {
        $result = match($operator) {
            '<' => $not ? $stored >= $searched : $stored < $searched,
            '<=' => $not ? $stored > $searched : $stored <= $searched,
            '>' => $not ? $stored <= $searched : $stored > $searched,
            '>=' => $not ? $stored < $searched : $stored >= $searched,
            '%=' => preg_match('/^.*' . $searched . '$/', $stored),
            '=%' => preg_match('/^' . $searched . '.*$/', $stored),
            '%%' => preg_match('/^.*' . $searched . '.*$/', $stored),
            default => null
        };

        if (null !== $result) {
            return $result;
        }

        switch ($operator) {   
            case '..':
                if (!is_array($searched) || count($searched) !== 2) {
                    throw new FelipaException('Condition value with operator `..` has to be a [min, max] array.');
                }
                return $not ? $stored < $searched[0] || $stored > $searched[1] : $stored >= $searched[0] && $stored <= $searched[1];
            
            case '#':
                if (!is_callable($searched)) {
                    throw new FelipaException('Condition value with operator `#` must be a callable function.');
                }
                return $not ? !$searched($stored) : $searched($stored);
            case '=':
            default:
                if (is_array($searched)) {
                    return $not ? !in_array($stored, $searched) : in_array($stored, $searched);
                }
                
                return $not ? $stored !== $searched : $stored === $searched;
        }
    }

    /**
     * Closes current connector and stored data in it.
     */
    public function close(): void
    {
        file_put_contents($this->getConnectorPath(), $this->encode($this->data));
    }

    /**
     * Saves changes into the database file.
     */
    public function __destruct() {
        if ($this->isOpened()) {
            $this->close();
        }
    }
}