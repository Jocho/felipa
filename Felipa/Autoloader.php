<?php

namespace Felipa;

class Autoloader {
    private const FILENAME_CACHE = '/.autoload.cache';
    private const FILENAME_JSON = '/.autoload.json';

    private static string $root = './';
    private static bool $cache = true;

    /**
     * Initializes autoloader class. Takes two arguments - root location of the project and switch for turning the
     * caching function on/off. When the cache function is turned on, the autoloader tries to load the file's location
     * from the cache. If it does not exist, the standard loading process is launched.
     * 
     * @param string $root - location of the project
     * @param bool $cache  - switch to turn the caching function on/off
     * @return void
     */
    public static function init(string $root = './', bool $cache = true): void
    {
        self::$root = $root;
        self::$cache = $cache;

        spl_autoload_register(fn (string $className) => self::load($className));
    }

    /**
     * Launches attempt to load give class based on its full classname.
     *
     * @param string $className - name of the class to be loaded
     */
    private static function load(string $className): void
    {
        $cname = $className;

        if (self::$cache) {
            $path = self::loadCachedPath($className);

            if (null !== $path) {
                require_once($path);
                return;
            }
        }

        $settings = self::loadSettings(self::$root . self::FILENAME_JSON);
        $classPath = self::$root . DIRECTORY_SEPARATOR;

        if (null !== $settings) {
            foreach ($settings as $prefix => $replacement) {
                if (0 === strpos($className, $prefix)) {
                    $className = str_replace($prefix, $replacement, $className);
                    break;
                }
            }
        }

        $classPath .= $className . '.php';
        $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $classPath);

        $path = file_exists($classPath) ? $classPath : null; 
        
        if (null === $path) {
            throw new FelipaException('Class `' . $className . '` not found.');
        }

        if (self::$cache) {
            self::cachePath($cname, $path);
        }

        require_once($path);
    }

    /**
     * Load autoload settings - aliases for paths when the namespace does not match exact file location.
     *
     * @param string $path - path to the `autoload.json` settings file
     */
    private static function loadSettings(string $path): ?array
    {
        static $settings = null;

        if (null === $settings) {
            $settings = file_exists($path) ? json_decode(file_get_contents($path), true) : null;
        }

        return $settings;
    }

    /**
     * Saves the exact file location into the cache file for faster search in the future.
     *
     * @param string $className - namespaced name of the class to be saved
     * @param string $path      - absolute physical path of the class location
     */
    private static function cachePath(string $className, string $path): void
    {
        $cache = [];

        if (file_exists(self::$root . self::FILENAME_CACHE)) {
            $cache = json_decode(file_get_contents(self::$root . self::FILENAME_CACHE), true) ?? [];
        }

        $cache[$className] = $path;

        file_put_contents(self::$root . self::FILENAME_CACHE, json_encode($cache));
    }

    /**
     * Tries to load the class file location from the cache file if cache file exists.
     *
     * @param string $className - name of the class to be loaded form the cache
     */
    private static function loadCachedPath(string $className): ?string
    {
        if (!file_exists(self::$root . self::FILENAME_CACHE)) {
            return null;
        }

        $cache = json_decode(file_get_contents(self::$root . self::FILENAME_CACHE), true);

        return $cache[$className] ?? null;
    }
}
