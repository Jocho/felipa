<?php
namespace Felipa;

class Response
{
    private const STREAM_DATA = ['id', 'event', 'data', 'retry'];

    private array $headers = [];

    private array $vars = [];

    private string $template = 'index';

    private string $path = ROOT . '/templates';

    private ResponseMethod $method = ResponseMethod::HTML;

    private int $responseCode = 200;

    /**
     * Sets the template variables and template path to load proper template files when response is called.
     *
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        if (isset($settings['template'])) {
            $this->vars = $settings['template']['vars'] ?? [];
            $this->path = $settings['template']['path'] ?? ROOT . '/templates';
        }
    }

    /**
     * Sets the template filename to be used for rendering the page results.
     *
     * @param string $templateName
     * @return void
     * @throws FelipaException
     */
    public function setTemplate(string $templateName)
    {
        $this->templateExists($templateName);

        $this->template = $templateName;
    }

    /**
     * Checks whether the file with given template name exists in the template path or not.
     *
     * @param string $templateName
     * @return void
     * @throws FelipaException
     */
    private function templateExists(string $templateName)
    {
        $path = $this->path . DIRECTORY_SEPARATOR . $templateName . '.html';

        if (!file_exists($path)) {
            throw new FelipaException('Template with name `' . $path . '` does not exist.');
        }
    }

    /**
     * Sets the template variable. The variable can be later used in the template itself.
     *
     * @param string $variable
     * @param [type] $value
     * @return void
     */
    public function set(string $variable, $value)
    {
        $this->vars[$variable] = $value;
    }

    /**
     * Sets the Content-Type header to text/html and renders the HTML file with all the variables stored in `$vars`
     * available for usage within the template file.
     *
     * @return void
     * @throws FelipaException
     */
    private function html()
    {
        $response = null;
        $this->templateExists($this->template);

        if (!count($this->headers)) {
            header('Content-Type: text/html');
        }

        ob_start();
        extract($this->vars);

        include($this->path . DIRECTORY_SEPARATOR . $this->template . '.html');
        $response = ob_get_clean();

        echo $response;
    }

    /**
     * Sets the Content-Type header to text/json and dumps all the variables stored in `$vars` array.
     *
     * @return void
     */
    private function json()
    {
        if (!count($this->headers)) {
            header('Content-Type: text/json');
        }

        echo json_encode($this->vars);
    }

    /**
     * Sets the Content-Type header to text/event-stream and launches the stream of data based on the content of the
     * `$vars` variable.
     *
     * @return void
     */
    private function stream()
    {
        if (!count($this->headers)) {
            header('Cache-Control: no-cache');
            header('Content-Type: text/event-stream');
        }

        // ob_start();

        foreach (self::STREAM_DATA as $key) {
            $responseData = isset($this->vars[$key]) ? $this->vars[$key] : null;

            if (!is_null($responseData)) {
                echo $key . ': ' . json_encode($responseData) . "\n";
            }
        }
        echo "\n";

        ob_end_flush();
        flush();
    }

    /**
     * Sets the Content-Type header to text/plain and returns content of the `$vars` variable pretty much the same way
     * as the `json()` method.
     *
     * @see json()
     * @return void
     */
    private function custom()
    {
        if (!count($this->headers)) {
            header('Content-Type: text/plain');
        }

        echo json_encode($this->vars);
    }

    /**
     * Constructs the response based on given method.
     *
     * @return void
     */
    public function makeResponse(Session $session): void
    {
        http_response_code($this->responseCode);

        if (isset($this->headers['Location']) && !empty($this->vars)) {
            $session->set('VARS', $this->vars);
        }
        
        if (!$session->isEmpty()) {    
            $session->commit();
        }

        foreach ($this->headers as $headerName => $headerValue) {
            header(implode(': ', [$headerName, $headerValue]));
        }

        match($this->method) {
            ResponseMethod::HTML => $this->html(),
            ResponseMethod::JSON => $this->json(),
            ResponseMethod::STREAM => $this->stream(),
            default => $this->custom(),
        };
    }

    /**
     * Sets the response type to HTML, stores variables into `$vars` array and prepares response code.
     *
     * @param array|null $vars
     * @param integer|null $responseCode
     * @return self
     */
    public function withHtml(?array $vars = [], ?int $responseCode = 200): self
    {
        $this->responseCode = $responseCode;
        $this->vars = array_merge($this->vars, $vars);
        $this->method = ResponseMethod::HTML;
        return $this;
    }

    /**
     * Sets the response type to JSON, stores variables into `$vars` array and prepares response code.
     *
     * @param array|null $vars
     * @param integer|null $responseCode
     * @return self
     */
    public function withJson(?array $vars = [], ?int $responseCode = 200): self
    {
        $this->responseCode = $responseCode;
        $this->vars = array_merge($this->vars, $vars);
        $this->method = ResponseMethod::JSON;
        return $this;
    }

    /**
     * Appends headers set in the argument into the list.
     *
     * @param array|null $headers
     * @return self
     */
    public function withHeaders(?array $headers = []): self
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }

    /**
     * Sets the response type to STREAM, stores variables into `$vars` array and prepares response code.
     *
     * @param array|null $vars
     * @param integer|null $responseCode
     * @return self
     */
    public function withStream(?array $vars = [], ?int $responseCode = 200): self
    {
        $this->responseCode = $responseCode;
        $this->vars = array_merge($this->vars, $vars);
        $this->method = ResponseMethod::STREAM;
        return $this;
    }

    /**
     * Sets the response type to CUSTOM, stores variables into `$vars` array and prepares response code.
     *
     * @param array|null $vars
     * @param integer|null $responseCode
     * @return self
     */
    public function withCustom(?array $vars = [], ?int $responseCode = 200): self
    {
        $this->responseCode = $responseCode;
        $this->vars = array_merge($this->vars, $vars);
        $this->method = ResponseMethod::CUSTOM;
        return $this;
    }

    /**
     * Returns the method of repsonse.
     *
     * @return ResponseMethod
     */
    public function getMethod(): ResponseMethod
    {
        return $this->method;
    }

    /**
     * Returns currently set response code.
     *
     * @return integer
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * Immediately runs GET redirection to a given path.
     *
     * @param string $path
     * @return self
     */
    public function redirect(string $path): self
    {
        $this->headers['Location'] = $path;
        return $this;
    }
}