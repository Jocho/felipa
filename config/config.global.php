<?php

use Felipa\Felipa;
use Felipa\Database;
use Example\Controller\ExampleController;
use Example\Repository\ExampleRepository;
use Example\Service\Service;

return [
    'database' => [
        'type' => Database::TYPE_JSON,
        'host' => ROOT . '/db'
    ],

    'dependencies' => [
        // controllers
        ExampleController::class => [ExampleRepository::class],

        // repositories
        ExampleRepository::class => [Database::class, Service::class],

        // database
        Database::class => [Felipa::CONFIG],

        // service
        Service::class => [Felipa::INSTANCE]
    ],

    'template' => [
        'path' => ROOT . '/templates',
        'vars' => []
    ],
];