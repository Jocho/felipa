<?php

use Felipa\Felipa;
use Felipa\Database;
use Example\Controller\ExampleController;
use Example\Repository\ExampleRepository;

return [
    'database' => [
        'type' => Database::TYPE_JSON,
        'host' => ROOT . '/db'
    ],

    'template' => [
        'path' => ROOT . '/templates',
        'vars' => []
    ]
];