<?php

use Felipa\Request;
use Felipa\Response;
use Example\Controller\ExampleController;

return [
    'GET  /'           => ExampleController::class . ':init',
    'GET  /form'       => ExampleController::class . ':form',
    'POST /form'       => ExampleController::class . ':save',

    'GET /redirect-me' => function (Request $req, Response $res): Response {
        $req->getSession()->set('pears', [4, 3, 2, 1]);

        return $res->redirect('/to-this')->withJson([
            'hello' => 'pear'
        ]);
    },
    'GET /to-this' => function (Request $req, Response $res): Response {
        return $res->withJson([
            'session' => $req->getSession()->all(),
            'requestVars' => $req->getVars()
        ]);
    },

    'GET /service-di' => ExampleController::class . ':serviceDI',

    '/note/{id}' => [
        'GET  /'      => ExampleController::class . ':detail',
        'POST /'      => ExampleController::class . ':save',
        'GET /remove' => ExampleController::class . ':remove',
    ],

    'GET  /*'          => function(Request $request, Response $response) {
        return $response->withJson([
            'status'  => 'error',
            'message' => 'Page not found.'
        ], 404);
    }
];
