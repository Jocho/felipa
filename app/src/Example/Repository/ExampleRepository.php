<?php
namespace Example\Repository;

use Felipa\Database;
use Felipa\FelipaException;
use Example\Model\ExampleModel;
use Example\Service\Service;

class ExampleRepository
{
    private Database $db;
    private Service $service;

    public function __construct(Database $db, Service $service) {
        $this->db = $db;
        $this->service = $service;

        $this->db->open('Example');
    }

    public function getService(): Service
    {
        return $this->service;
    }

    public function getList()
    {
        $response = [];

        foreach ($this->db->get() as $id => $row) {
            $row['id'] = $id;
            $response[] = ExampleModel::fromArray($row);
        }
        return $response;
    }

    public function getDetail(string $id): ?ExampleModel
    {
        $data = $this->db->get($id);

        $data['id'] = $id;

        return !empty($data) ? ExampleModel::fromArray($data) : null;
    }

    public function save(ExampleModel $model): void
    {
        if (null === $model->getId()) {
            $this->db->add($model->toArray());
            return;
        }
        
        $this->db->set($model->getId(), $model->toArray());
    }

    public function remove(string $id): void
    {
        $this->db->del($id);
    }
}
