<?php
namespace Example\Controller;

use Example\Model\ExampleModel;
use Felipa\Database;
use Felipa\Templater;
use Felipa\FelipaException;
use Felipa\Request;
use Felipa\Response;
use Example\Repository\ExampleRepository;

class ExampleController
{
    private ExampleRepository $repo;

    private Templater $html;

    public function __construct(
        ExampleRepository $repo
    ) {
        $this->repo = $repo;
        $this->html = new Templater(ROOT . '/app/src/Example/Html/');
    }

    public function init(Response $response): Response
    {
        $list = $this->repo->getList();

        return $response->withHtml([
            'body' => $this->html
                ->setSnippet('example.init')
                ->setAll(['list' => $list])
                ->render()
            ]);
    }

    public function serviceDI(Request $request, Response $response): Response
    {
        if (empty($request->getQuery())) {
            return $response->withHtml([
                'body' => 'No request query found. Visit <a href="?timestamp=' . time() . '">this link</a> to see dependency injection in practice.'
            ]);
        }

        return $response->withHtml([
            'body' => join('', [
                'Contents of request query obtained in controller and directly from application via dependency injection.',
                '<pre>',
                'from request in Controller: ' . json_encode($request->getQuery()),
                "\n",
                'from request in Service:    ' . json_encode($this->repo->getService()->getRequestVars()),
                '</pre>',
                '<a href="/service-di">Go back</a> | <a href="/">Go to homepage</a>'
            ])
        ]);
    }

    public function form(Request $request, Response $response): Response
    {
        return $response->withHtml(['body' => $this->drawForm()]);
    }

    public function detail(Request $request, Response $response): Response
    {
        $model = $this->repo->getDetail($request->getVars()['id']);

        if (empty($model)) {
            throw new FelipaException('Record with given ID does not exist.');
        }

        return $response->withHtml(['body' => $this->drawForm($model)]);
    }

    public function save(Request $request, Response $response): Response
    {
        $data = $request->getInputs();

        $data['id'] = (isset($request->getVars()['id'])) ? $request->getVars()['id'] : null;

        $model = ExampleModel::fromArray($data);

        $this->repo->save($model);

        return $response->redirect('/');
    }

    public function remove(Request $request, Response $response): Response
    {
        $this->repo->remove($request->getVars()['id']);
        return $response->redirect('/');
    }

    private function drawForm(?ExampleModel $model = null): string
    {
        $this->html
            ->setSnippet('example.form')
            ->setAll([
                'idx' => !empty($model) ? '/note/' . $model->getId() : '',
                'number' => !empty($model) ? $model->getNumber() : 0,
                'text' => !empty($model) ? $model->getText() : '',
            ]);
        return $this->html->render();
    }

}
