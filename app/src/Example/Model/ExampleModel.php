<?php

namespace Example\Model;

class ExampleModel
{
    private ?string $id;

    private ?int $number;

    private ?string $text;

    public function __construct(
        ?string $id,
        ?int $number,
        ?string $text
    ) {
        $this->id = $id;
        $this->number = $number;
        $this->text = $text;
    }

    public static function fromArray(array $data)
    {
        return new self($data['id'], $data['number'], $data['text']);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'text' => $this->text,
        ];
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getText(): ?string
    {
        return $this->text;
    }
}
