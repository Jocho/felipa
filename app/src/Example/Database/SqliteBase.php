<?php

namespace Example\Database;

use PDO;

class SqliteBase extends PDO
{
    public function __construct(array $settings)
    {
        $dbs = $settings['database'];
        $connection = 'sqlite:' . $dbs['host'];

        parent::__construct($connection, $dbs['username'], $dbs['password']);
    }
}