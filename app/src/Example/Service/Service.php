<?php

namespace Example\Service;

use Felipa\Felipa;

final class Service
{
    private Felipa $app;

    public function __construct(Felipa $app)
    {
        $this->app = $app;
    }

    public function getRequestVars(): array
    {
        return $this->app->getRequest()->getQuery();
    }
}